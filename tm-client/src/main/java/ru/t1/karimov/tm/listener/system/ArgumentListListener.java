package ru.t1.karimov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.IListener;
import ru.t1.karimov.tm.event.ConsoleEvent;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument list.";

    @NotNull
    public static final String NAME = "arguments";

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@argumentListListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[ARGUMENTS]");
        for(final IListener listener: listeners) {
            if(listener == null) continue;
            @Nullable final String argument = listener.getArgument();
            if(argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
