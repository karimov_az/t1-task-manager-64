package ru.t1.karimov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.karimov.tm.event.ConsoleEvent;
import ru.t1.karimov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    public static final String NAME = "task-complete-by-id";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setId(id);
        taskEndpoint.completeTaskById(request);
    }

}
