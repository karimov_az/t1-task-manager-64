package ru.t1.karimov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.karimov.tm.api.IReceiverService;
import ru.t1.karimov.tm.listener.EntityListener;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void start() {
        receiverService.receive(entityListener);
    }

}
